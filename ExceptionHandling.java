import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try {
                System.out.print("Inputkan pembilang    : ");
                int pembilang = scanner.nextInt();

                System.out.print("Inputkan penyebut     : ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("Hasil pembagian       : " + hasil);

                validInput = true;
            } catch (InputMismatchException e) {
                System.out.println("Input harus berupa bilangan bulat,silahkan masukkan bilangan yang benar");
                scanner.nextLine(); 
            } catch (ArithmeticException e) {
                System.out.println("Terjadi kesalahan pembagian Penyebut tidak boleh bernilai 0,silahkan masukkan bilangan yang benar");
                scanner.nextLine(); 
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut;
    }
}
